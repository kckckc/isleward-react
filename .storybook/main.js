const { mergeConfig } = require('vite');

module.exports = {
	// stories: ["../src/**/*.stories.tsx"],
	stories: ['../src'],

	// Add any Storybook addons you want here: https://storybook.js.org/addons/
	addons: [
		'@storybook/addon-essentials',
		// '@storybook/addon-jest'
		// TODO: try interactions
	],

	// Vite builder
	core: {
		builder: '@storybook/builder-vite',
	},
	viteFinal: async (config) => {
		return mergeConfig(config, {
			//
		});
	},
};
