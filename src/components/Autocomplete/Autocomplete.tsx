import classNames from 'classnames';
import React, { useEffect, useRef, useState } from 'react';
import scrollIntoView from 'scroll-into-view-if-needed';
import Input from '../Input';
import styles from './Autocomplete.module.scss';
import { AutocompleteProps } from './Autocomplete.types';

const Autocomplete: React.FC<AutocompleteProps> = ({
	id,
	value: userInput = '',
	setValue: setUserInput = () => {},
	onSubmit,
	options = [],
	selectMode = false,
	placeholder = '',
	indicator = '',
	indicatorColor = 'red',
	// indicatorTooltip = '',
	centered = false,
}) => {
	const [showOptions, setShowOptions] = useState(false);
	const [filteredOptions, setFilteredOptions] = useState([]);
	const [activeOption, setActiveOption] = useState(0);
	const [isChanged, setChanged] = useState(false);

	const containerRef = useRef<HTMLDivElement>(null);

	const updateScroll = (e) => {
		if (e)
			scrollIntoView(e, {
				scrollMode: 'if-needed',
				block: 'nearest',
				inline: 'nearest',
			});
	};

	useEffect(() => {
		const onClick = (e) => {
			if (
				containerRef.current &&
				!containerRef.current.contains(e.target)
			) {
				setShowOptions(false);
				setChanged(false);
				setActiveOption(0);
			}
		};

		document.addEventListener('mousedown', onClick);
		return () => {
			document.removeEventListener('mousedown', onClick);
		};
	}, [containerRef]);

	const onChange = (e) => {
		const input = e.currentTarget.value;
		setFilteredOptions(
			options.filter(
				(option) =>
					option.toLowerCase().indexOf(input.toLowerCase()) > -1
			)
		);
		setActiveOption(0);
		setShowOptions(true);
		setUserInput(input);
		setChanged(true);
	};
	const onKeyDown = (e) => {
		let optionsList;
		if (selectMode && userInput === '') optionsList = options;
		else optionsList = filteredOptions;

		if (e.key === 'Enter') {
			// Select and close
			if ((selectMode && showOptions) || (showOptions && userInput)) {
				setActiveOption(0);
				setShowOptions(false);
				setChanged(false);
				setUserInput(optionsList[activeOption]);
			}

			if (onSubmit) {
				onSubmit();
			}

			e.preventDefault();
		} else if (e.key === 'ArrowUp') {
			if (activeOption > 0) {
				setActiveOption(activeOption - 1);
				setChanged(true);
			}
			e.preventDefault();
		} else if (e.key === 'ArrowDown') {
			if (activeOption < optionsList.length - 1) {
				setActiveOption(activeOption + 1);
				setChanged(true);
			}
			e.preventDefault();
		} else if (e.key === 'Escape') {
			// Close without selecting

			if (selectMode) return;

			setActiveOption(0);
			setShowOptions(false);
			setChanged(false);
			e.preventDefault();
		} else if (e.key === 'Tab') {
			// Tab key should select the currently selected input

			if ((selectMode && showOptions) || (showOptions && userInput)) {
				setActiveOption(0);
				setShowOptions(false);
				setChanged(false);
				if (isChanged) {
					setUserInput(optionsList[activeOption]);
				}
			}
		}
	};
	const onClick = (e) => {
		setFilteredOptions([]);
		setActiveOption(0);
		setShowOptions(false);
		setChanged(false);
		setUserInput(e.currentTarget.innerText);

		if (onSubmit) {
			onSubmit();
		}
	};
	const onFocus = () => {
		if (selectMode) {
			setShowOptions(true);
			setFilteredOptions(options);
			if (selectMode) {
				setActiveOption(
					Math.max(
						0,
						options.findIndex((a) => a === userInput)
					)
				);
			}
		}
	};
	const shouldShowOptions =
		(selectMode && showOptions) || (showOptions && userInput);

	let optionsList;
	if (selectMode && userInput === '') optionsList = options;
	else optionsList = filteredOptions;

	return (
		<div
			className={classNames(
				styles.Autocomplete,
				centered && styles.centered
			)}
			role="combobox"
			aria-controls={`${id}-listbox`}
			aria-expanded={showOptions}
			ref={containerRef}
		>
			<Input
				value={userInput}
				onChange={onChange}
				onKeyDown={onKeyDown}
				onFocus={onFocus}
				aria-controls={`${id}-listbox`}
				placeholder={placeholder}
				indicator={indicator}
				indicatorColor={indicatorColor}
				// indicatorTooltip={indicatorTooltip}
				centered={centered}
			/>
			{shouldShowOptions && (
				<ul
					className={styles.options}
					role="listbox"
					id={`${id}-listbox`}
				>
					{optionsList.map((a, i) => {
						let className;
						const selected = i === activeOption;
						if (selected) className = styles.active;

						return (
							<li
								className={className}
								key={a}
								onClick={onClick}
								onKeyDown={onKeyDown}
								role="option"
								aria-selected={selected}
								ref={selected ? updateScroll : null}
							>
								{a}
							</li>
						);
					})}
				</ul>
			)}
		</div>
	);
};

export default Autocomplete;
