import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';

import ItemImage from './ItemImage';
import { ItemImageProps } from './ItemImage.types';

describe('ItemImage', () => {
	let props: ItemImageProps;

	beforeEach(() => {
		props = {
			//
		};
	});

	const renderComponent = () => render(<ItemImage {...props} />);

	it('renders empty correctly', () => {
		const cpn = renderComponent();

		expect(cpn.asFragment()).toMatchSnapshot();
	});

	it('renders empty with a slot correctly', () => {
		props.slot = 'head';
		const cpn = renderComponent();

		expect(cpn.asFragment()).toMatchSnapshot();
	})

	it('renders an item correctly', () => {
		props.item = {
			sprite: [0, 0]
		}
		const cpn = renderComponent();

		expect(cpn.asFragment()).toMatchSnapshot();
	});

	it('renders eq text correctly', () => {
		props.quantity = {
			showEq: true
		}
		props.item = {
			sprite: [0, 0],
			eq: true
		}
		renderComponent();

		expect(screen.getByText('EQ')).toBeVisible();
	});

	it('renders qs text correctly', () => {
		props.quantity = {
			showQs: true
		}
		props.item = {
			sprite: [0, 0],
			quickslot: true
		}
		renderComponent();

		expect(screen.getByText('QS')).toBeVisible();
	});

	it('renders new text correctly', () => {
		props.quantity = {
			showNew: true
		}
		props.item = {
			sprite: [0, 0],
			new: true
		}
		renderComponent();

		expect(screen.getByText('NEW')).toBeVisible();
	});

	it('renders stack size correctly', () => {
		props.quantity = {
			showQuantity: true
		}
		props.item = {
			sprite: [0, 0],
			quantity: 5
		}
		renderComponent();

		expect(screen.getByText('5')).toBeVisible();
	});

	it('renders stack size with forceQuantity correctly', () => {
		props.quantity = {
			showQuantity: true,
			forceQuantity: true
		}
		props.item = {
			sprite: [0, 0]
		}
		renderComponent();

		expect(screen.getByText('1')).toBeVisible();
	});

	it('renders custom quantities correctly', () => {
		props.quantity = {
			quantityFunction: () => 'mylabel'
		}
		props.item = {
			sprite: [0, 0]
		}
		renderComponent();

		expect(screen.getByText('mylabel')).toBeVisible();
	});

});
