import React from 'react';

export interface CheckboxProps {
	checked: boolean,
	id?: string,
	children?: React.ReactNode
	onChange?: () => void,
	delta?: number
}
