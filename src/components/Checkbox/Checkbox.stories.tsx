import React from 'react';
import { Meta, Story } from '@storybook/react';
import { useArgs } from '@storybook/client-api';

import Checkbox from './Checkbox';
import { CheckboxProps } from './Checkbox.types';

export default {
    title: 'Checkbox',
    component: Checkbox,
    argTypes: {
        onChange: {
            action: 'clicked'
        }
    }
} as Meta;

const Template: Story<CheckboxProps> = (args) => <Checkbox id='story' {...args} />;

// For some reason children doesn't work in these args, so use an additional label arg
const InteractiveTemplate: Story<CheckboxProps & { label: string }> = ({ label, ...args }) => {
    const [_, updateArgs] = useArgs();
    const onChange = () => {
        updateArgs({ checked: !args.checked });
    }

    return <Checkbox id='story' {...args} onChange={onChange}>{label}</Checkbox>
}

export const Default: Story<CheckboxProps> = Template.bind({});
Default.args = {
    children: 'Checkbox'
}

export const Checked: Story<CheckboxProps> = Template.bind({});
Checked.args = {
    checked: true,
    children: 'Checked Checkbox'
}

export const Unchecked: Story<CheckboxProps> = Template.bind({});
Unchecked.args = {
    checked: false,
    children: 'Unchecked Checkbox'
}

export const Interactive: Story<CheckboxProps & { label: string }> = InteractiveTemplate.bind({});
Interactive.args = {
    label: 'Checkbox'
}
