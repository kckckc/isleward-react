// Generated with util/create-component.js
import React, { useCallback } from 'react';

import { InputProps } from './Input.types';

import styles from './Input.module.scss';
import classNames from 'classnames';

const Input: React.FC<InputProps> = ({
	value = '',
	setValue = () => {},
	placeholder = '',
	indicator = '',
	indicatorColor = 'red',
	// indicatorTooltip: '',
	'aria-controls': ariaControls,
	centered = false,
	onChange,
	onKeyDown,
	onFocus,
}) => {
	const handleChange = useCallback(
		(e) => {
			if (onChange) {
				onChange(e);
			} else {
				setValue(e.currentTarget.value);
			}
		},
		[onChange]
	);

	return (
		<div className={classNames(styles.Input, centered && styles.centered)}>
			<input
				type="text"
				className={styles.input}
				spellCheck="false"
				onChange={handleChange}
				onKeyDown={onKeyDown}
				onFocus={onFocus}
				value={value}
				placeholder={placeholder}
				aria-controls={ariaControls}
			/>
			{indicator !== '' && (
				<div
					className={classNames(
						styles.indicator,
						styles[indicatorColor]
					)}
				>
					{indicator}
				</div>
			)}
		</div>
	);
};

export default Input;
