import { ChangeEventHandler } from 'react';

export interface InputProps {
	value?: string
	setValue?: (string) => void
	onChange?: ChangeEventHandler
	onKeyDown?: (KeyboardEvent) => void
	onFocus?: () => void
	'aria-controls'?: string
	centered?: boolean

	placeholder?: string

	indicator?: string
	indicatorColor?: 'red' | 'blue' | 'green' | 'yellow'
	indicatorTooltip?: string
}
