export interface PanelOrderProps {
	value: number;
	setValue: (val: number) => void;
}
