import React from 'react';

import { FilterReferenceProps } from './FilterReference.types';

import styles from './FilterReference.module.scss';

const FilterReference: React.FC<FilterReferenceProps> = ({ onClose }) => {
	// TODO move linkbutton to its own component?

	const handleClick = (e) => {
		e.preventDefault();

		if (onClose) onClose();
	};

	return (
		<div className={styles.FilterReference}>
			<div className={styles.highlight}>
				Search Reference{' '}
				<button
					type="button"
					onClick={handleClick}
					className={styles['link-button']}
				>
					(close)
				</button>
			</div>

			<br />

			<div>
				Search terms can be grouped with parentheses and can be combined
				using <code>and</code> / <code>or</code>.
			</div>
			<div>
				They can be negated with a minus, exclamation point, or with{' '}
				<code>not</code> (<code>-slot:head</code> /{' '}
				<code>!slot:head</code> / <code>not slot:head</code>).
			</div>

			<br />

			<div className={styles.highlight}>Features</div>

			<br />

			<div>
				Fuzzy search for item names and types: <code>skyblasson</code>,{' '}
				<code>axxe</code>
			</div>
			<div>
				Multi-word search: <code>&quot;family heirloom&quot;</code>
			</div>
			<div>
				Lots of filters: <code>is:equipped</code>,{' '}
				<code>is:weapon</code>, <code>is:rune</code>
			</div>
			<div>
				Negation: <code>-is:equipped</code>, <code>!is:weapon</code>,{' '}
				<code>not is:rune</code>
			</div>
			<div>
				Logical operators: <code>is:epic OR is:legendary</code>
			</div>
			<div>
				Grouping:{' '}
				<code>
					(is:legend and is:weapon) or ((is:epic or is:legend) and
					is:armor)
				</code>
			</div>
			<div>
				Comparison operators: <code>stat:str {'>'} 7</code>
			</div>

			<br />

			<div className={styles.highlight}>Command List</div>

			<br />

			<div>
				<code>is:common is:white</code>
			</div>
			<div>
				<code>is:magic is:green</code>
			</div>
			<div>
				<code>is:rare is:blue</code>
			</div>
			<div>
				<code>is:epic is:purple</code>
			</div>
			<div>
				<code>is:legendary is:orange</code>
			</div>
			<div>
				<code>is:spell</code> (weapons and runes)
			</div>
			<div>
				<code>is:weapon</code>
			</div>
			<div>
				<code>is:rune</code>
			</div>
			<div>
				<code>is:quest</code>
			</div>
			<div>
				<code>is:material</code>
			</div>
			<div>
				<code>is:maxlevel is:20</code>
			</div>
			<div>
				<code>is:equipped is:eq</code>
			</div>
			<div>
				<code>is:quickslot is:qs</code>
			</div>
			<div>
				<code>is:equipment</code>
			</div>
			<div>
				<code>is:armor</code>
			</div>
			<div>
				<code>is:tool</code>
			</div>
			<div>
				<code>is:augmented</code>
			</div>
			<div>
				<code>is:maxaugs</code>
			</div>
			<div>
				<code>is:noaugs</code>
			</div>
			<div>
				<code>not:weapon not:armor not:equipped</code> etc (available
				for every <code>is:</code>)
			</div>
			<div>
				<code>slot:head slot:neck slot:offhand</code> etc
			</div>
			<div>
				<code>slot:onehanded slot:1h</code>
			</div>
			<div>
				<code>slot:twohanded slot:2h</code>
			</div>
			<div>
				<code>slot:weapon</code>
			</div>
			<div>
				<code>slot:anyhand</code>
			</div>
			<div>
				<code>stat:str {'>'} 5</code>
			</div>
			<div>
				<code>basestat:int {'>'} 5</code>
			</div>
			<div>
				<code>aug:dex {'>'} 5</code>
			</div>
			<div>
				<code>aug:count {'<'} 3</code>
			</div>
			<div>
				<code>level: = 15</code> (pre-reduction; items without level
				will be 0)
			</div>
			<div>
				<code>roll:damage {'>'} 20</code>
			</div>
			<div>
				<code>roll:radius {'>='} 2</code>
			</div>
			<div>
				<code>requires:str</code>
			</div>
			<div>
				<code>requires:dex {'<'} 15</code>
			</div>
			<div>
				<code>requires:level {'<'} 20</code>
			</div>
		</div>
	);
};

export default FilterReference;
