import React from 'react';
import { Meta, Story } from '@storybook/react';
import Panel from './Panel';
import { PanelProps } from './Panel.types';
import { CM } from '../ContextMenu';

export default {
	title: 'Panel',
	component: Panel,
} as Meta;

const Template: Story<PanelProps> = (args) => (
	<div
		style={{
			position: 'absolute',
			left: '0px',
			right: '0px',
			top: '0px',
			bottom: '0px',
		}}
	>
		<Panel {...args} />
	</div>
);

export const Default: Story<PanelProps> = Template.bind({});
Default.args = {
	children: (
		<div style={{ margin: '8px' }}>
			some body text.
			<br />
			some body text.
			<br />
			some body text.
			<br />
		</div>
	),
	header: 'Some Header Text',
};

export const WithContextMenu: Story<PanelProps> = Template.bind({});
WithContextMenu.args = {
	children: (
		<div style={{ margin: '8px' }}>
			some body text.
			<br />
			some body text.
			<br />
			some body text.
			<br />
		</div>
	),
	header: 'Some Header Text',
	contextMenu: (
		<CM>
			<CM.Label>some label</CM.Label>
			<CM.Button>some button</CM.Button>
		</CM>
	),
};

export const WithClose: Story<PanelProps> = Template.bind({});
WithClose.args = {
	children: (
		<div style={{ margin: '8px' }}>
			some body text.
			<br />
			some body text.
			<br />
			some body text.
			<br />
		</div>
	),
	header: 'Some Header Text',
	onClose: () => {},
};

export const OpenCentered: Story<PanelProps> = Template.bind({});
OpenCentered.args = {
	children: (
		<div style={{ margin: '8px' }}>
			some body text.
			<br />
			some body text.
			<br />
			some body text.
			<br />
		</div>
	),
	header: 'Some Header Text',
	openCentered: true,
};

export const SavePosition: Story<PanelProps> = Template.bind({});
SavePosition.args = {
	children: (
		<div style={{ margin: '8px' }}>
			some body text.
			<br />
			some body text.
			<br />
			some body text.
			<br />
		</div>
	),
	header: 'Some Header Text',
	saveAs: 'test',
};
