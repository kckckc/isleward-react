import { CSSProperties, ReactNode } from 'react';

// CSS Variables:
// --iwdutil-color-panel
// --iwdutil-color-panel-header
// --iwdutil-color-close
// --iwdutil-color-close-hover

export interface PanelProps {
	className?: string;
	style?: CSSProperties;
	header?: ReactNode;
	children?: ReactNode;
	contextMenu?: ReactNode;
	onClose?: () => void;
	openCentered?: boolean;
	clamped?: boolean;
	saveAs?: string;
}
