import React from 'react';
import { SlotType } from '..';
import { ItemQuantityFilters } from '../..';
import { IWDItem } from '../../isleward';

export interface ItemSlotProps {
	item?: IWDItem;
	tooltip?: boolean;
	contextMenu?: React.ReactNode;
	slot?: SlotType;
	usable?: boolean; // TODO ?
	selected?: boolean;
	quantity?: ItemQuantityFilters;
	noBackground?: boolean;
	dropShadow?: boolean;
	onClick?: () => void;
}
