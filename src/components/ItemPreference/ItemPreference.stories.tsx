import React from 'react';
import { Meta, Story } from '@storybook/react';

import ItemPreference from './ItemPreference';
import ItemImage from '../ItemImage';
import { ItemPreferenceProps } from './ItemPreference.types';
import { ItemImageProps } from '../ItemImage/ItemImage.types';
import { IWDItem } from '../../isleward';

import styles from '../stories.module.scss';

export default {
	title: 'ItemPreference',
} as Meta;

const Template: Story<ItemImageProps> = (args) => <ItemImage {...args} />;
const AllQualTemplate: Story<{ item: IWDItem; usable: boolean }> = (props) => (
	<div
		style={{
			display: 'flex',
			flexDirection: 'row',
		}}
	>
		<Template item={{ ...props.item, quality: 0 }} usable={props.usable} />
		<Template item={{ ...props.item, quality: 1 }} usable={props.usable} />
		<Template item={{ ...props.item, quality: 2 }} usable={props.usable} />
		<Template item={{ ...props.item, quality: 3 }} usable={props.usable} />
		<Template item={{ ...props.item, quality: 4 }} usable={props.usable} />
	</div>
);
const QualProviderTemplate: Story<{
	item: IWDItem;
	usable: boolean;
	value: ItemPreferenceProps;
}> = ({ item, value, usable }) => (
	<ItemPreference.Provider value={value}>
		<AllQualTemplate usable={usable} item={item} />
	</ItemPreference.Provider>
);
const UnusableProviderTemplate: Story<{
	item: IWDItem;
	value: ItemPreferenceProps;
}> = ({ item, value }) => (
	<ItemPreference.Provider value={value}>
		<Template item={item} usable={false} />
	</ItemPreference.Provider>
);
const AllQualPrefTemplate: Story<{
	item: IWDItem;
	usable: boolean;
	value: ItemPreferenceProps;
}> = (props) => (
	<div
		style={{
			display: 'flex',
			flexDirection: 'column',
			rowGap: '16px',
		}}
	>
		<QualProviderTemplate
			usable={true}
			item={props.item}
			value={{ qualityIndicators: 'off' }}
		/>
		<QualProviderTemplate
			usable={true}
			item={props.item}
			value={{ qualityIndicators: 'bottom' }}
		/>
		<QualProviderTemplate
			usable={true}
			item={props.item}
			value={{ qualityIndicators: 'border' }}
		/>
		<QualProviderTemplate
			usable={true}
			item={props.item}
			value={{ qualityIndicators: 'background' }}
		/>
	</div>
);
const AllUnusablePrefTemplate: Story<{
	item: IWDItem;
	value: ItemPreferenceProps;
}> = (props) => (
	<div
		style={{
			display: 'flex',
			flexDirection: 'row',
		}}
	>
		<UnusableProviderTemplate
			item={props.item}
			value={{ unusableIndicators: 'off' }}
		/>
		<UnusableProviderTemplate
			item={props.item}
			value={{ unusableIndicators: 'top' }}
		/>
		<UnusableProviderTemplate
			item={props.item}
			value={{ unusableIndicators: 'border' }}
		/>
		<UnusableProviderTemplate
			item={props.item}
			value={{ unusableIndicators: 'background' }}
		/>
	</div>
);

const AllRow: Story<{
	item: IWDItem;
	unusableIndicators: ItemPreferenceProps['unusableIndicators'];
}> = (props) => (
	<div
		style={{
			display: 'flex',
			flexBasis: '50%',
			flexDirection: 'column',
		}}
	>
		<span className={styles.StoryLabel}>
			unusable: {props.unusableIndicators}
		</span>
		<QualProviderTemplate
			usable={false}
			item={props.item}
			value={{
				qualityIndicators: 'off',
				unusableIndicators: props.unusableIndicators,
			}}
		/>
		<QualProviderTemplate
			usable={false}
			item={props.item}
			value={{
				qualityIndicators: 'bottom',
				unusableIndicators: props.unusableIndicators,
			}}
		/>
		<QualProviderTemplate
			usable={false}
			item={props.item}
			value={{
				qualityIndicators: 'border',
				unusableIndicators: props.unusableIndicators,
			}}
		/>
		{props.unusableIndicators !== 'background' && (
			<QualProviderTemplate
				usable={false}
				item={props.item}
				value={{
					qualityIndicators: 'background',
					unusableIndicators: props.unusableIndicators,
				}}
			/>
		)}
	</div>
);
const AllTemplate: Story<{ item: IWDItem }> = (props) => (
	<div
		style={{
			display: 'flex',
			flexDirection: 'column',
			height: '50px',
			flexWrap: 'wrap',
			rowGap: '16px',
		}}
	>
		<AllRow item={props.item} unusableIndicators={'off'} />
		<AllRow item={props.item} unusableIndicators={'top'} />
		<AllRow item={props.item} unusableIndicators={'border'} />
		<AllRow item={props.item} unusableIndicators={'background'} />
	</div>
);

export const Quality: Story<{ item: IWDItem }> = AllQualPrefTemplate.bind({});
Quality.args = {
	item: {
		sprite: [0, 0],
	},
};

export const Unusable: Story<{ item: IWDItem }> = AllUnusablePrefTemplate.bind(
	{}
);
Unusable.args = {
	item: {
		sprite: [0, 0],
	},
};

export const Combination: Story<{ item: IWDItem }> = AllTemplate.bind({});
Combination.args = {
	item: {
		sprite: [0, 0],
	},
};
