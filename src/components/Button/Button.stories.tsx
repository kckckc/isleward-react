import React from 'react';
import { Meta, Story } from '@storybook/react';

import Button from './Button';
import { ButtonProps } from './Button.types';

export default {
	title: 'Button',
	component: Button,
	argTypes: {
		onClick: {
			action: 'clicke'
		}
	}
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const Default: Story<ButtonProps> = Template.bind({});
Default.args = {
	children: 'Button Text'
}

export const Negative: Story<ButtonProps> = Template.bind({});
Negative.args = {
	children: 'Button Text',
	negative: true
}

export const Disabled: Story<ButtonProps> = Template.bind({});
Disabled.args = {
	children: 'Button Text',
	disabled: true
}
