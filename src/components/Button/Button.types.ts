export interface ButtonProps {
	children?: React.ReactNode,
	onClick?: () => void,
	className?: string,
	negative?: boolean
	disabled?: boolean
}
