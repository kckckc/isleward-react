import classNames from 'classnames';
import React from 'react';
import { createPortal } from 'react-dom';
import { PortalProps } from './Portal.types';
import styles from './Portal.module.scss';

const Portal: React.FC<PortalProps> = ({
	children,
	className,
	element = 'div',
}) => {
	const [container] = React.useState(() => {
		const el = document.createElement(element);
		el.classList.add(classNames(styles.portal, className));
		return el;
	});

	React.useEffect(() => {
		document.body.appendChild(container);
		return () => {
			document.body.removeChild(container);
		};
	}, []);

	return createPortal(children, container);
};

export default Portal;
