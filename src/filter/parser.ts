/* eslint @typescript-eslint/naming-convention: "off",
   @typescript-eslint/no-unused-vars: "off",
   @typescript-eslint/no-use-before-define: "off",
   no-return-assign: "off"
*/
// TODO: maybe fix these linting issues?

// https://lisperator.net/pltut

function InputStream(input: string) {
	let pos = 0;
	let line = 1;
	let col = 0;

	const next = () => {
		const ch = input.charAt(pos);
		pos += 1;
		if (ch === '\n') {
			line += 1;
			col = 0;
		} else {
			col += 1;
		}
		return ch;
	};
	const peek = () => input.charAt(pos);
	const eof = () => peek() === '';
	const croak = (msg) => {
		throw new Error(`${msg} (${line}:${col})`);
	};

	return {
		next,
		peek,
		eof,
		croak,
	};
}

function TokenStream(input: ReturnType<typeof InputStream>) {
	let current = null;
	const keywords = ['true', 'false'];

	const is_keyword = (x) => keywords.includes(x);
	const is_digit = (ch) => /[0-9]/i.test(ch);
	const is_id_start = (ch) => /[a-z_]/i.test(ch);
	const is_id = (ch) => is_id_start(ch) || ':0123456789'.indexOf(ch) >= 0;
	const is_op_char = (ch) => '-&|=<>!'.indexOf(ch) >= 0;
	const is_punc = (ch) => '()'.indexOf(ch) >= 0;
	const is_whitespace = (ch) => ' \t\n'.indexOf(ch) >= 0;
	const read_while = (predicate) => {
		let str = '';
		while (!input.eof() && predicate(input.peek())) {
			str += input.next();
		}
		return str;
	};
	const read_number = () => {
		let has_dot = false;
		const number = read_while((ch) => {
			if (ch === '.') {
				if (has_dot) return false;
				has_dot = true;
				return true;
			}
			return is_digit(ch);
		});
		return { type: 'num', value: parseFloat(number) };
	};
	const read_escaped = (end) => {
		let escaped = false;
		let str = '';
		input.next();
		while (!input.eof()) {
			const ch = input.next();
			if (escaped) {
				str += ch;
				escaped = false;
			} else if (ch === '\\') {
				escaped = true;
			} else if (ch === end) {
				break;
			} else {
				str += ch;
			}
		}
		return str;
	};
	const read_ident = () => {
		const id = read_while((ch) => {
			if (ch === ':') return false;

			return is_id(ch);
		});

		if (input.peek() === ':') {
			input.next();

			let right;
			if (input.peek() === '"') {
				right = read_escaped('"');
			} else {
				right = read_while(is_id);
			}

			return {
				type: 'var',
				value: `${id}:${right}`,
			};
		}

		if (id.toLowerCase() === 'and' || id.toLowerCase() === 'or') {
			return {
				type: 'op',
				value: id.toLowerCase(),
			};
		}
		if (id.toLowerCase() === 'not') {
			return {
				type: 'op',
				value: '!',
			};
		}

		return {
			// type: is_keyword(id) ? 'kw' : 'var',
			type: 'str',
			value: id,
		};
	};
	const read_string = () => ({ type: 'str', value: read_escaped('"') });
	// let skip_comment
	const read_next = () => {
		read_while(is_whitespace);

		if (input.eof()) return null;

		const ch = input.peek();

		if (ch === '"') return read_string();
		if (is_digit(ch)) return read_number();
		if (is_id_start(ch)) return read_ident();
		if (is_punc(ch)) {
			return {
				type: 'punc',
				value: input.next(),
			};
		}
		if (is_op_char(ch)) {
			return {
				type: 'op',
				value: read_while(is_op_char),
			};
		}
		return input.croak(`Can't handle character: ${ch}`);
	};
	const peek = () => current || (current = read_next());
	const next = () => {
		const tok = current;
		current = null;
		return tok || read_next();
	};
	const eof = () => peek() === null;

	return {
		next,
		peek,
		eof,
		croak: input.croak,
	};
}

function parse(input) {
	const PRECEDENCE = {
		'||': 6,
		or: 6,
		'&&': 7,
		and: 7,
		'=': 11,
		'==': 11,
		'!=': 11,
		'<': 12,
		'>': 12,
		'<=': 12,
		'>=': 12,
	};

	const is_punc = (ch) => {
		const tok = input.peek();
		return tok && tok.type === 'punc' && (!ch || tok.value === ch) && tok;
	};
	const is_kw = (kw) => {
		const tok = input.peek();
		return tok && tok.type === 'kw' && (!kw || tok.value === kw) && tok;
	};
	const is_op = (op) => {
		const tok = input.peek();
		return tok && tok.type === 'op' && (!op || tok.value === op) && tok;
	};
	const skip_punc = (ch) => {
		if (is_punc(ch)) input.next();
		else input.croak(`Expecting punctuation: "${ch}"`);
	};
	const skip_kw = (kw) => {
		if (is_kw(kw)) input.next();
		else input.croak(`Expecting keyword: "${kw}"`);
	};
	const skip_op = (op) => {
		if (is_op(op)) input.next();
		else input.croak(`Expecting operator: "${op}"`);
	};
	const unexpected = () => {
		input.croak(`Unexpected token: ${JSON.stringify(input.peek())}`);
	};
	const maybe_binary = (left, my_prec) => {
		const tok = is_op(false); // pass false to check for any op
		if (tok) {
			const his_prec = PRECEDENCE[tok.value];
			if (his_prec > my_prec) {
				input.next();
				return maybe_binary({
					type: tok.value === ':' ? 'filter' : 'binary',
					operator: tok.value,
					left,
					right: maybe_binary(parse_atom(), his_prec),
				}, my_prec);
			}
		}
		return left;
	};
	const delimited = (start, stop, separator, parser) => {
		const a = [];
		let first = true;
		skip_punc(start);
		while (!input.eof()) {
			if (is_punc(stop)) break;
			if (first) first = false; else skip_punc(separator);
			if (is_punc(stop)) break;
			a.push(parser());
		}
		skip_punc(stop);
		return a;
	};
	const parse_varname = () => {
		const name = input.next();
		if (name.type !== 'var') input.croak('Expecting variable name');
		return name.value;
	};
	const parse_bool = () => ({
		type: 'bool',
		value: input.next().value === 'true',
	});
	let parse_atom = () => {
		if (is_punc('(')) {
			input.next();
			const exp = parse_expression();
			skip_punc(')');
			return exp;
		}

		if (is_op('!') || is_op('-')) {
			input.next();
			return {
				type: 'not',
				body: parse_expression(),
			};
		}

		if (is_kw('true') || is_kw('false')) return parse_bool();

		const tok = input.next();
		if (tok.type === 'var' || tok.type === 'num' || tok.type === 'str') {
			return tok;
		}
		return unexpected();
	};
	const parse_toplevel = () => {
		const prog = [];
		while (!input.eof()) {
			prog.push(parse_expression());
		}
		return {
			type: 'prog',
			prog,
		};
	};
	let parse_expression = () => maybe_binary(parse_atom(), 0);

	return parse_toplevel();
}

const generateAST = (code) => {
	if (!code || code.length < 1) {
		return null;
	}

	const ast = parse(TokenStream(InputStream(code)));

	if (ast.prog.length < 1) {
		return null;
	}

	return ast;
};

export default generateAST;
