import Fuse from 'fuse.js';
import stringSimilarity from 'string-similarity';
import generateAST from './parser';

const parseFilter = (filterString) => {
	try {
		return generateAST(filterString);
	} catch (e) {
		return null;
	}
};

const optionsIs = [
	{
		names: ['common', 'white'],
		evaluate: ({ item }) => item.quality === 0,
	}, {
		names: ['magic', 'green', 'uncommon'],
		evaluate: ({ item }) => item.quality === 1,
	}, {
		names: ['rare', 'blue'],
		evaluate: ({ item }) => item.quality === 2,
	}, {
		names: ['epic', 'purple'],
		evaluate: ({ item }) => item.quality === 3,
	}, {
		names: ['legendary', 'orange'],
		evaluate: ({ item }) => item.quality === 4,
	},

	{
		names: ['spell'],
		evaluate: ({ item }) => !!item.spell,
	}, {
		names: ['weapon'],
		evaluate: ({ item }) => !!(item.spell && !item.ability),
	}, {
		names: ['rune'],
		evaluate: ({ item }) => !!(item.spell && item.ability),
	},

	{
		names: ['quest'],
		evaluate: ({ item }) => !!(item.quest),
	},
	{
		names: ['material'],
		evaluate: ({ item }) => !!(item.material),
	},

	{
		names: ['maxlevel', 'level20', '20'],
		evaluate: ({ item }) => (item.originalLevel ?? item.level) === 20,
	},

	{
		names: ['equipped', 'eq'],
		evaluate: ({ item }) => !!item.eq,
	},
	{
		names: ['quickslot', 'quickslotted', 'qs'],
		evaluate: ({ item }) => item.has('quickSlot'),
	},

	{
		names: ['equipment'],
		evaluate: ({ item }) => !!item.slot,
	}, {
		names: ['armor'],
		evaluate: ({ item }) => ['head', 'neck', 'chest', 'hands', 'finger', 'waist', 'legs', 'feet', 'trinket', 'offHand'].includes(item.slot),
	}, {
		names: ['tool'],
		evaluate: ({ item }) => item.slot === 'tool',
	},

	{
		names: ['augmented'],
		evaluate: ({ item }) => (item.power ?? 0) > 0,
	},
	{
		names: ['maxaugs'],
		evaluate: ({ item }) => (item.power ?? 0) === 3,
	},
	{
		names: ['noaugs'],
		evaluate: ({ item }) => item.slot && (item.power ?? 0) === 3,
	},
];
const searchIs = new Fuse(optionsIs, {
	keys: ['names'],
	threshold: 0.4,
});

const optionsSlot = [
	{
		names: ['head', 'helmet'],
		evaluate: ({ item }) => item.slot === 'head',
	},
	{
		names: ['neck', 'necklace'],
		evaluate: ({ item }) => item.slot === 'neck',
	},
	{
		names: ['chest'],
		evaluate: ({ item }) => item.slot === 'chest',
	},
	{
		names: ['hands', 'gloves'],
		evaluate: ({ item }) => item.slot === 'hands',
	},
	{
		names: ['finger', 'ring'],
		evaluate: ({ item }) => item.slot === 'finger',
	},
	{
		names: ['waist', 'belt'],
		evaluate: ({ item }) => item.slot === 'waist',
	},
	{
		names: ['legs', 'pants'],
		evaluate: ({ item }) => item.slot === 'legs',
	},
	{
		names: ['feet', 'boots'],
		evaluate: ({ item }) => item.slot === 'feet',
	},
	{
		names: ['trinket'],
		evaluate: ({ item }) => item.slot === 'trinket',
	},
	{
		names: ['onehanded', 'oneh', '1h', '1handed', '1hand'],
		evaluate: ({ item }) => item.slot === 'oneHanded',
	},
	{
		names: ['twohanded', 'twoh', '2h', '2handed', '2hand'],
		evaluate: ({ item }) => item.slot === 'twoHanded',
	},
	{
		names: ['offhand', 'shield', 'tome'],
		evaluate: ({ item }) => item.slot === 'offHand',
	},
	{
		names: ['weapon'],
		evaluate: ({ item }) => item.slot === 'oneHanded' || item.slot === 'twoHanded',
	},
	{
		names: ['anyhand'],
		evaluate: ({ item }) => item.slot === 'oneHanded' || item.slot === 'twoHanded' || item.slot === 'offHand',
	},
	{
		names: ['tool'],
		evaluate: ({ item }) => item.slot === 'tool',
	},
];
const searchSlot = new Fuse(optionsSlot, {
	keys: ['names'],
	threshold: 0.4,
});

const optionsStat = [
	{ name: 'vit', aliases: ['vitality'] },
	{ name: 'regenHp', aliases: ['health regeneration', 'health regen', 'hp regen'] },
	{ name: 'manaMax', aliases: ['maximum mana', 'max mana'] },
	{ name: 'regenMana', aliases: ['mana regeneration', 'mana regen'] },
	{ name: 'str', aliases: ['strength'] },
	{ name: 'int', aliases: ['intellect'] },
	{ name: 'dex', aliases: ['dexterity'] },
	{ name: 'armor', aliases: [] },
	{ name: 'blockAttackChance', aliases: ['attack block', 'block attack'] },
	{ name: 'blockSpellChance', aliases: ['spell block', 'block spell'] },
	{ name: 'dodgeAttackChance', aliases: ['attack dodge', 'dodge attack'] },
	{ name: 'dodgeSpellChance', aliases: ['spell dodge', 'dodge spell'] },
	{ name: 'addCritChance', aliases: ['global crit chance', 'global crit rate', 'crit chance', 'crit rate'] },
	{ name: 'addCritMultiplier', aliases: ['global crit multiplier', 'global crit mult', 'crit mult'] },
	{ name: 'addAttackCritChance', aliases: ['attack crit chance', 'attack crit rate'] },
	{ name: 'addAttackCritMultiplier', aliases: ['attack crit multiplier', 'attack crit mult', 'attack mult'] },
	{ name: 'addSpellCritChance', aliases: ['spell crit chance', 'spell crit rate'] },
	{ name: 'addSpellCritMultiplier', aliases: ['spell crit multiplier', 'spell crit mult', 'spell mult'] },
	{ name: 'magicFind', aliases: ['magic find', 'item quality', 'quality', 'increased item quality'] },
	{ name: 'itemQuantity', aliases: ['quantity', 'increased item quantity'] },
	{ name: 'sprintChance', aliases: ['sprint', 'sprint chance'] },
	{ name: 'allAttributes', aliases: ['allattr', 'all', 'to all attributes'] },
	{ name: 'xpIncrease', aliases: ['bonus xp', 'xp', 'additional xp per kill'] },
	{ name: 'lvlRequire', aliases: ['level requirement reduction', 'lvlreduct', 'level reduct', 'level req'] },
	{ name: 'elementArcanePercent', aliases: ['increased arcane damage', 'arcane damage'] },
	{ name: 'elementFrostPercent', aliases: ['increased frost damage', 'frost damage'] },
	{ name: 'elementFirePercent', aliases: ['increased fire damage', 'fire damage'] },
	{ name: 'elementHolyPercent', aliases: ['increased holy damage', 'holy damage'] },
	{ name: 'elementPoisonPercent', aliases: ['increased poison damage', 'poison damage'] },
	{ name: 'physicalPercent', aliases: ['increased physical damage', 'physical damage'] },
	{ name: 'elementPercent', aliases: ['increased elemental damage', 'elemental damage'] },
	{ name: 'spellPercent', aliases: ['increased spell damage', 'spell damage'] },
	{ name: 'elementAllResist', aliases: ['all resistance', 'all resist'] },
	{ name: 'elementArcaneResist', aliases: ['arcane resistance', 'arcane resist'] },
	{ name: 'elementFrostResist', aliases: ['frost resistance', 'frost resist'] },
	{ name: 'elementFireResist', aliases: ['fire resistance', 'fires resist'] },
	{ name: 'elementHolyResist', aliases: ['holy resistance', 'holy resist'] },
	{ name: 'elementPoisonResist', aliases: ['poison resistance', 'poison resist'] },
	{ name: 'attackSpeed', aliases: ['attack speed'] },
	{ name: 'castSpeed', aliases: ['cast speed', 'spell speed'] },
	{ name: 'lifeOnHit', aliases: ['life gained on hit', 'life on hit'] },
	{ name: 'auraReserveMultiplier', aliases: ['aura mana reservation multiplier', 'aura reserve', 'aura mana'] },
	{ name: 'weight', aliases: ['lb'] },
	{ name: 'catchChance', aliases: ['extra catch chance', 'catch chance'] },
	{ name: 'catchSpeed', aliases: ['faster catch speed', 'catch speed'] },
	{ name: 'fishRarity', aliases: ['higher fish rarity', 'fish rarity', 'fish quality'] },
	{ name: 'fishWeight', aliases: ['increased fish weight', 'extra weight'] },
	{ name: 'fishItems', aliases: ['extra chance to hook items', 'item chance'] },
];
const searchStat = new Fuse(optionsStat, {
	keys: ['name', 'aliases'],
	threshold: 0.4,
});

const evaluateIs = (item, filter, env, value) => {
	const res = searchIs.search(value);
	const action = res[0].item;

	if (!action) {
		throw new Error(`Can't handle var is: ${JSON.stringify(filter)}`);
	}

	return action.evaluate({ item });
};

const evaluateVar = (item, filter, env) => {
	const originalKey = filter.value.split(':')[0];
	const originalValue = filter.value.split(':')[1];
	const key = originalKey.toLowerCase();
	const value = originalValue.toLowerCase();

	if (key === 'is') {
		return evaluateIs(item, filter, env, value);
	}

	if (key === 'not') {
		return !evaluateIs(item, filter, env, value);
	}

	if (key === 'slot') {
		const res = searchSlot.search(value);
		const action = res[0].item;

		if (!action) {
			throw new Error(`Can't handle var slot: ${JSON.stringify(filter)}`);
		}

		return action.evaluate({ item });
	}

	if (key === 'roll') {
		if (!item.spell) return null;

		const { bestMatch } = stringSimilarity
			.findBestMatch(value, Object.keys(item.spell.values).map((r) => r.toLowerCase()));

		if (bestMatch.rating < 0.5) { return null; }

		return Object.entries(item.spell.values)
			.find((v) => v[0].toLowerCase() === bestMatch.target)[1];
	}

	if (key === 'level') {
		return (item.originalLevel ?? item.level ?? 0);
	}

	if (key === 'requires') {
		const { bestMatch } = stringSimilarity
			.findBestMatch(value, ['level', 'lvl', 'dex', 'str', 'int']);

		if (bestMatch.rating < 0.5) { throw new Error(`Unknown item requirement: ${value}`); }

		const { target } = bestMatch;
		if (target === 'level' || target === 'lvl') {
			return item.level ?? 0;
		}
		if (target === 'dex') {
			return ((item.requires ?? []).find((s) => s.stat === 'dex') ?? { value: 0 }).value;
		}
		if (target === 'str') {
			return ((item.requires ?? []).find((s) => s.stat === 'str') ?? { value: 0 }).value;
		}
		if (target === 'int') {
			return ((item.requires ?? []).find((s) => s.stat === 'int') ?? { value: 0 }).value;
		}

		throw new Error(`Unknown item requirement: ${value}`);
	}

	if (key === 'augcount' || key === 'power' || key === 'numaugs') {
		if (value === '0' || value === '1' || value === '2' || value === '3') {
			const target = parseInt(value, 10);

			return (item.stats || item.enchantedStats || item.implicitStats)
				&& ((item.power ?? 0) === target);
		}

		return item.power ?? 0;
	}

	if (key === 'aug' || key === 'augs' || key === 'augment' || key === 'augments' || key === 'augstat' || key === 'augstats') {
		if (value === '0' || value === '1' || value === '2' || value === '3') {
			const target = parseInt(value, 10);

			return (item.stats || item.enchantedStats || item.implicitStats)
				&& ((item.power ?? 0) === target);
		}
		if (value === 'count' || value === 'num') {
			return item.power ?? 0;
		}

		const res = searchStat.search(value);
		const stat = res[0].item;

		if (!stat) {
			throw new Error(`Can't handle var aug stat: ${JSON.stringify(filter)}`);
		}

		return (item.enchantedStats ?? {})[stat.name] ?? 0;
	}

	if (key === 'stat') {
		const res = searchStat.search(value);
		const stat = res?.[0]?.item;

		if (!stat) {
			throw new Error(`Can't handle var stat: ${JSON.stringify(filter)}`);
		}

		return (item.stats ?? {})[stat.name] ?? 0;
	}

	if (key === 'basestat') {
		const res = searchStat.search(value);
		const stat = res?.[0]?.item;

		if (!stat) {
			throw new Error(`Can't handle var stat: ${JSON.stringify(filter)}`);
		}

		return ((item.stats ?? {})[stat.name] ?? 0) - ((item.enchantedStats ?? {})[stat.name] ?? 0);
	}

	throw new Error(`Can't handle var: ${JSON.stringify(filter)}`);
};

const evaluateBinary = (item, filter, env) => {
	/* eslint-disable-next-line @typescript-eslint/no-use-before-define */
	const a = evaluate(item, filter.left, env);
	/* eslint-disable-next-line @typescript-eslint/no-use-before-define */
	const b = evaluate(item, filter.right, env);

	if (a === null || b === null) {
		return null;
	}

	const op = filter.operator;

	const num = (x) => {
		if (typeof x !== 'number') throw new Error(`Expected number but got ${x}`);
		return x;
	};

	if (op === 'or' || op === '||') return a || b;
	if (op === 'and' || op === '&&') return a && b;
	if (op === '>') return num(a) > num(b);
	if (op === '>=') return num(a) >= num(b);
	if (op === '<') return num(a) < num(b);
	if (op === '<=') return num(a) <= num(b);
	if (op === '=') return num(a) === num(b);
	if (op === '==') return num(a) === num(b);
	if (op === '!=') return num(a) !== num(b);

	throw new Error(`Can't handle operator: ${JSON.stringify(filter)}`);
};

const evaluateStr = (item, filter) => {
	// string search

	const keys = ['name', 'type'];

	let b = filter.value;
	b = b.toLowerCase();

	// if there is a 2+ character substring match OR the distance is close enough
	return (b.length >= 2 && keys.some((k) => {
		let a = item[k];
		if (!a) return false;

		a = a.toLowerCase();

		return a.indexOf(b) >= 0;
	})) || keys.some((k) => {
		let a = item[k];
		if (!a) return false;
		a = a.toLowerCase();

		return stringSimilarity.compareTwoStrings(a, b) > 0.5;
	});
};

const evaluateNum = (item, filter) => filter.value;

let evaluate = (item, filter, env) => {
	if (filter.type === 'str') {
		return evaluateStr(item, filter);
	}
	if (filter.type === 'num') {
		return evaluateNum(item, filter);
	}
	if (filter.type === 'not') {
		return !(evaluate(item, filter.body, env));
	}
	if (filter.type === 'var') {
		return evaluateVar(item, filter, env);
	}
	if (filter.type === 'binary') {
		return evaluateBinary(item, filter, env);
	}

	throw new Error(`Can't handle expression: ${JSON.stringify(filter)}`);
};

const checkFilter = (item, filter, settings, isFirst) => {
	if (!filter) return true;

	for (let i = 0; i < filter.prog.length; i += 1) {
		try {
			const res = evaluate(item, filter.prog[i], {
				settings,
			});
			if (!res) {
				return false;
			}
		} catch (e) {
			// TODO: output somewhere nice
			if (isFirst) {
				/* eslint-disable-next-line no-console */
				console.error(e, item);
			}
		}
	}

	return true;
};

const filterItems = (items, filter, settings) => items
	.filter((i, index) => checkFilter(i, filter, settings, index === 0));

export { parseFilter, filterItems };
