const QUALITIES = [
	'Common',
	'Magic',
	'Rare',
	'Epic',
	'Legendary',
];
const QUALITIES_SHORT = [
	'Cmmn',
	'Magc',
	'Rare',
	'Epic',
	'Lgnd',
];
function translateQuality(quality: number, short: boolean = false) {
	return (short ? QUALITIES_SHORT : QUALITIES)[quality] ?? 'Unknown';
}

export default translateQuality;
