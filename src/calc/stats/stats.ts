import merge from 'deepmerge';
import { SPIRITS } from '../consts';

const statScales = {
	vitToHp: 10,
	strToArmor: 1,
	intToMana: (1 / 6),
	dexToDodge: (1 / 12),
};

const baseStats = {
	mana: 20,
	manaMax: 20,

	manaReservePercent: 0,

	hp: 5,
	hpMax: 5,
	xpTotal: 0,
	xp: 0,
	xpMax: 0,
	level: 1,
	str: 0,
	int: 0,
	dex: 0,
	magicFind: 0,
	itemQuantity: 0,
	regenHp: 0,
	regenMana: 5,

	addCritChance: 0,
	addCritMultiplier: 0,
	addAttackCritChance: 0,
	addAttackCritMultiplier: 0,
	addSpellCritChance: 0,
	addSpellCritMultiplier: 0,

	critChance: 5,
	critMultiplier: 150,
	attackCritChance: 0,
	attackCritMultiplier: 0,
	spellCritChance: 0,
	spellCritMultiplier: 0,

	armor: 0,
	vit: 0,

	blockAttackChance: 0,
	blockSpellChance: 0,
	dodgeAttackChance: 0,
	dodgeSpellChance: 0,

	attackSpeed: 0,
	castSpeed: 0,

	elementArcanePercent: 0,
	elementFrostPercent: 0,
	elementFirePercent: 0,
	elementHolyPercent: 0,
	elementPoisonPercent: 0,
	physicalPercent: 0,

	elementPercent: 0,
	spellPercent: 0,

	elementArcaneResist: 0,
	elementFrostResist: 0,
	elementFireResist: 0,
	elementHolyResist: 0,
	elementPoisonResist: 0,

	elementAllResist: 0,

	sprintChance: 0,

	xpIncrease: 0,

	lifeOnHit: 0,

	// Fishing stats
	catchChance: 0,
	catchSpeed: 0,
	fishRarity: 0,
	fishWeight: 0,
	fishItems: 0,
};

export type StatType = keyof typeof baseStats;

export type StatValuePartialType = Partial<Record<StatType, number>>;

export class Stats {
	isPlayer: boolean;

	values: Record<StatType, number>;

	spirit?: keyof typeof SPIRITS;

	constructor(level, isPlayer = false, spirit = null) {
		this.values = merge({}, baseStats);
		this.values.level = level;

		this.isPlayer = isPlayer;
		this.spirit = spirit;

		if (isPlayer) {
			this.calcXpMax();
			this.addLevelAttributes();
			this.calcHpMax();
		}
	}

	addStat(stat: StatType | 'allAttributes' | 'lvlRequire', value) {
		const { values } = this;

		if (['lvlRequire', 'allAttributes'].indexOf(stat) === -1) {
			values[stat] += value;
		}

		if (['addCritChance', 'addAttackCritChance', 'addSpellCritChance'].indexOf(stat) > -1) {
			let morphStat = stat.substr(3);
			morphStat = morphStat[0].toLowerCase() + morphStat.substr(1);
			this.addStat(morphStat as StatType, (0.05 * value));
		} else if (['addCritMultiplier', 'addAttackCritMultiplier', 'addSpellCritMultiplier'].indexOf(stat) > -1) {
			let morphStat = stat.substr(3);
			morphStat = morphStat[0].toLowerCase() + morphStat.substr(1);
			this.addStat(morphStat as StatType, value);
		} else if (stat === 'vit') {
			this.addStat('hpMax', (value * statScales.vitToHp));
		} else if (stat === 'allAttributes') {
			['int', 'str', 'dex'].forEach((s: StatType) => {
				this.addStat(s, value);
			});
		} else if (stat === 'elementAllResist') {
			['arcane', 'frost', 'fire', 'holy', 'poison'].forEach((s) => {
				const element = `element${s[0].toUpperCase() + s.substr(1)}Resist`;
				this.addStat(element as StatType, value);
			});
		} else if (stat === 'elementPercent') {
			['arcane', 'frost', 'fire', 'holy', 'poison'].forEach((s) => {
				const element = `element${s[0].toUpperCase() + s.substr(1)}Percent`;
				this.addStat(element as StatType, value);
			});
		} else if (stat === 'str') {
			this.addStat('armor', (value * statScales.strToArmor));
		} else if (stat === 'int') {
			this.addStat('manaMax', (value * statScales.intToMana));
		} else if (stat === 'dex') {
			this.addStat('dodgeAttackChance', (value * statScales.dexToDodge));
		}
	}

	calcXpMax() {
		const { level } = this.values;
		this.values.xpMax = (level * 5) + Math.floor(level * 10 * (level ** 2.2)) - 5;
	}

	calcHpMax() {
		const spirit = SPIRITS[this.spirit];

		const initialHp = spirit ? spirit.values.hpMax : 32.7;
		const increase = spirit ? spirit.values.hpPerLevel : 32.7;

		this.values.hpMax = initialHp + (((this.values.level || 1) - 1) * increase);
	}

	addLevelAttributes(singleLevel = false) {
		if (!['owl', 'bear', 'lynx'].includes(this.spirit)) return;

		const { gainStats } = SPIRITS[this.spirit];
		const count = singleLevel ? 1 : this.values.level;

		Object.keys(gainStats).forEach((s) => {
			this.addStat(s as StatType, gainStats[s] * count);
		});
	}

	// takeDamage

	// getHp
}
