import { StatType, StatValuePartialType } from './calc/stats/stats';

export type IWDItem = {
	id?: number
	pos?: number
	level?: number
	originalLevel?: number

	quality?: number
	worth?: number
	worthText?: string
	quantity?: number
	cd?: number
	cdMax?: number
	uses?: number

	power?: number
	implicitStats?: ({ stat: StatType, value: number })[]
	stats?: StatValuePartialType
	enchantedStats?: StatValuePartialType

	ability?: boolean
	spell?: {
		name?: string
		type?: string
		rolls?: {
			[key: string]: number
		}
		values?: {
			[key: string]: number
		}
		random?: {
			[key: string]: [number, number]
		}
		quality?: number
		castTimeMax?: number
		cdMax?: number
		statMult?: number
		statType?: string
		useWeaponRange?: boolean
	}

	factions?: ({
		id?: string
		tier?: number
		tierName?: string
		name?: string
		noEquip?: boolean
	})[]

	effects?: ({
		factionId?: string
		properties?: {
			[key: string]: number | string
		}
		rolls?: {
			[key: string]: number | string
		}
		text?: string
		type?: string
	})[]

	requires?: ({ stat: string, value: number })[]

	name?: string
	type?: string
	slot?: string
	description?: string
	equipSlot?: string
	runeSlot?: number

	action?: string
	useText?: string

	sprite?: [number, number]
	spritesheet?: string
	spriteSize?: number

	eq?: boolean
	quickslot?: boolean
	quickSlot?: boolean
	new?: boolean
	material?: boolean
	quest?: boolean
	consumable?: boolean
	infinite?: boolean
	skin?: boolean
	active?: boolean
	noDestroy?: boolean
	noDrop?: boolean
	noSalvage?: boolean
	noAugment?: boolean
};
