import React from 'react';
import ReactDOM from 'react-dom';
import { toBlob, toPng } from 'html-to-image';
import ItemTooltip from '../components/ItemTooltip';

export default function generateTooltip(item) {
	// Create container for tooltip
	const container = document.createElement('div');

	// Render the tooltip component with the item
	// TODO options
	ReactDOM.render(<ItemTooltip
		item={item}
		pos={{ x: 4, y: 4 }} // for border shadow to (not) show? otherwise it shows partially
	/>, container, async () => {
		// TODO: figure out how to not have this flash when adding/removing
		document.body.appendChild(container);

		// Grab the tooltip
		const tt = container.children[0];

		try {
			// Use html-to-image to get an image blob
			const blob = await toBlob(container, {
				quality: 1,
				pixelRatio: 1,
				width: tt.clientWidth,
				height: tt.clientHeight,
			});

			// Copy to clipboard (kinda experimental/unsupported API?)
			await navigator.clipboard.write([
				new ClipboardItem({
					[blob.type]: blob,
				} as unknown as Record<string, ClipboardItemData>),
			]);
		} catch (e) {
			// Something went wrong

			// Maybe try just opening the image?
			const dataUrl = await toPng(container, {
				quality: 1,
				pixelRatio: 1,
				width: tt.clientWidth,
				height: tt.clientHeight,
			});

			const img = new Image();
			img.src = dataUrl;

			const w = window.open('');
			w.document.write(img.outerHTML);
			w.document.close();
		} finally {
			// Remove the tooltip
			document.body.removeChild(container);
		}
	});
}
