import deepmerge from 'deepmerge';
import { IWDItem } from '../isleward';
import { translateStatName, translateStatValue, translateQuality } from '../calc';

const capitalize = (str) => str.slice(0, 1).toUpperCase() + str.slice(1);
const formatStat = (k, v) => `${translateStatValue(k, v)} ${translateStatName(k)}`;

function generateText(_item: IWDItem) {
	// Make a copy; delete props as used so we can see what's extra
	const item: IWDItem = deepmerge({}, _item);

	// Delete useless info

	delete item.id;
	delete item.pos;

	delete item.sprite;
	delete item.spritesheet;

	delete item.eq;
	delete item.new;
	delete item.runeSlot;
	delete item.quickSlot;

	delete item.cd;
	delete item.cdMax; // game knowledge
	delete item.infinite; // game knowledge

	delete item.worth; // Not sure if/where to display

	// Game knowledge
	// delete item.action;
	delete item.description;
	delete item.equipSlot;
	delete item.slot;

	// Bugged on digested crystals
	delete (item as any).chance;
	delete (item as any).magicFind;

	// Header

	const qualText = (item.quality === 0 || item.quality) ? translateQuality(item.quality) : '';
	delete item.quality;

	const levelText = (item.originalLevel ?? item.level) ? ` Lv${item.originalLevel ?? item.level}${item.originalLevel ? `(${item.level})` : ''}` : '';
	delete item.level;
	delete item.originalLevel;

	let typeText = '';
	if (item.type) {
		if (item.type !== item.name) {
			typeText = capitalize(item.type);
		}

		// Delete anyway
		delete item.type;
	} else if (item.material) {
		typeText = 'Crafting Material';
		delete item.material;
	} else if (item.quest) {
		typeText = 'Quest Item';
		delete item.quest;
	}
	if (typeText !== '') typeText = ` ${typeText}`;

	const powerText = (item.power) ? ` ${'+'.repeat(item.power)}` : '';
	delete item.power;

	let headerParenText = qualText + levelText + typeText + powerText;
	if (headerParenText.length > 0) {
		headerParenText = ` (${headerParenText})`;
	}

	// Stats

	const statLines = [];

	if (item.implicitStats) {
		item.implicitStats.forEach((o) => {
			const { stat, value } = o;
			statLines.push(`  i ${formatStat(stat, value)}`);
		});
		delete item.implicitStats;
	}
	if (item.stats) {
		Object.entries(item.stats).forEach(([stat, value]) => {
			const augValue = (item.enchantedStats ?? {})[stat] ?? 0;
			const useValue = value - augValue;
			if (useValue === 0) {
				return;
			}

			statLines.push(`    ${formatStat(stat, useValue)}`);
		});
		delete item.stats;
	}
	if (item.enchantedStats) {
		Object.entries(item.enchantedStats).forEach((o) => {
			const [stat, value] = o;
			statLines.push(`  + ${formatStat(stat, value)}`);
		});
	}
	delete item.enchantedStats;

	// Requires

	const reqLines = [];
	if (item.requires) {
		item.requires.forEach((r) => reqLines.push(formatStat(r.stat, r.value)));
		delete item.requires;
	}

	if (item.factions) {
		item.factions.forEach((f) => {
			reqLines.push(`${f.name}: ${f.tierName}`);
		});
		delete item.factions;
	}

	const reqText = reqLines.length ? `Requires ${reqLines.join(', ')}` : '';

	// Spells

	let spellLines = [];
	if (item.spell) {
		spellLines = Object.entries(item.spell.values).map((r) => `    ${r[1]} ${r[0]}`);
		// Append the damage quality if we're not a rune
		if (!item.ability) {
			spellLines[spellLines.length - 1] = `${spellLines[spellLines.length - 1]} (${translateQuality(item.spell.quality)})`;
		}
		// Forget about other spell info like name, element, manaCost, etc (known by community);
		delete item.spell;
	}
	delete item.ability;

	// Extras

	let nameText = item.name;
	if (item.quantity) {
		nameText = `${item.quantity}x ${nameText}`;
		delete item.quantity;
	}
	delete item.name;

	const flags = [];
	if (item.noDestroy) flags.push('destroy');
	if (item.noDrop) flags.push('drop');
	if (item.noSalvage) flags.push('salvage');
	if (item.noAugment) flags.push('augment');
	delete item.noDestroy;
	delete item.noDrop;
	delete item.noSalvage;
	delete item.noAugment;
	const flagText = flags.length ? `Can't ${flags.join(', ')}` : '';

	const actionText = item.action ? `Action: ${item.action}` : '';
	const useText = item.useText ? `Action: ${item.useText}` : '';
	delete item.action;
	delete item.useText;

	const effectLines = [];
	if (item.effects) {
		item.effects.forEach((e) => {
			effectLines.push(capitalize(e.text ?? (`Effect: ${e.type}`)));
		});
		delete item.effects;
	}

	let extraText = '';
	if (JSON.stringify(item) !== '{}') {
		extraText = `\n\nEXTRA DATA (you can delete this, but an issue at https://gitlab.com/kckckc/isleward-util/-/issues/new with a screenshot of the item and this data would be helpful!): ${JSON.stringify(item)}`;
	}

	// Build body
	const body = [
		statLines.length && statLines,
		spellLines.length && spellLines,
		effectLines.length && effectLines,
		actionText.length && [actionText],
		useText.length && [useText],
		reqText.length && [reqText],
		flagText.length && [flagText],
	].filter((a) => !!a).map((g) => g.join('\n')).join('\n');

	return `${nameText}${headerParenText}${body.length > 0 ? '\n' : ''}${body}${extraText}`;
}

export default generateText;
