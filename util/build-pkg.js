const fs = require('fs');
const path = require('path');

const pathTo = (...args) => path.join(__dirname, '..', ...args);

const source = fs.readFileSync(pathTo('package.json')).toString('utf-8');
const pkg = JSON.parse(source);
pkg.scripts = {};
pkg.devDependencies = {};

// Fix paths
['main', 'module', 'types'].forEach((field) => {
	if (pkg[field].startsWith('dist/')) {
		pkg[field] = pkg[field].slice(5);
	}
});

fs.writeFileSync(
	pathTo('dist', 'package.json'),
	Buffer.from(JSON.stringify(pkg, null, 2), 'utf-8')
);
fs.writeFileSync(
	pathTo('dist', 'version.txt'),
	Buffer.from(pkg.version, 'utf-8')
);

fs.copyFileSync(pathTo('.npmignore'), pathTo('dist/.npmignore'));
fs.copyFileSync(pathTo('README.md'), pathTo('dist/README.md'));
