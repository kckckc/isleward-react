module.exports = (componentName) => ({
	content: `// Generated with util/create-component.js
@use '../typography' as *;
@use '../variables' as *;

.${componentName} {
	@include font-defaults;
}
`,
	extension: `.module.scss`,
});
