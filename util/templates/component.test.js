module.exports = (componentName) => ({
	content: `// Generated with util/create-component.js
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { composeStories } from '@storybook/testing-react';
import * as stories from './${componentName}.stories';

const { Default } = composeStories(stories);

describe('${componentName}', () => {
	// it('does something', () => {
	// 	render(<Default />);
	//
	// 	fireEvent(...);
	//
	// 	expect(screen.getByText('asdf')).toBeVisible();
	// });

	test.todo('write tests for ${componentName}');
});
`,
	extension: `.test.tsx`
});

