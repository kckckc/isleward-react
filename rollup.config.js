import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import copy from 'rollup-plugin-copy';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import styles from 'rollup-plugin-styles';
import typescript from 'rollup-plugin-typescript2';
import renameNodeModules from 'rollup-plugin-rename-node-modules';

const pkg = require('./package.json');

const extensions = ['.js', '.jsx', '.ts', '.tsx'];

export default {
	input: './src/index.ts',

	external: ['react', 'react-dom'],

	plugins: [
		peerDepsExternal(),
		nodeResolve({ extensions, browser: true }),
		commonjs(),
		typescript({
			useTsconfigDeclarationDir: true,
			tsconfigOverride: {
				exclude: [
					'node_modules',
					'dist',
					'storybook-static',
					'src/**/*.test.tsx',
					'src/**/*.stories.tsx',
				],
			},
		}),
		styles({
			modules: true,
			mode: [
				'inject',
				{
					treeshakeable: true,
				},
			],
		}),
		copy({
			targets: [
				// Partials for reuse
				{
					src: 'src/components/_variables.scss',
					dest: 'dist',
					rename: 'components/_variables.scss',
				},
				{
					src: 'src/components/_typography.scss',
					dest: 'dist',
					rename: 'components/_typography.scss',
				},
			],
		}),
		renameNodeModules('ext', false),
	],

	output: [
		{
			format: 'cjs',
			file: pkg.main,
		},
		{
			format: 'esm',
			dir: 'dist',
			preserveModules: true,
			preserveModulesRoot: 'src',
		},
	],
};
